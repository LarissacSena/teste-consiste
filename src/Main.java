import java.io.*;
import java.text.DecimalFormat;

import java.nio.charset.StandardCharsets;

public class Main {
    public static void main(String[] args) {
        String path = "src/arquivos/dataset.CSV";
        String line;
        DecimalFormat df = new DecimalFormat("#.##");

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8))) {
            try (FileWriter writer = new FileWriter("src/arquivos/mauraLarissaCostaSena.txt")) {

                // Pular cabeçalho
                br.readLine();

                while ((line = br.readLine()) != null) {
                    String[] values = line.split(";");
                    String firstName = values[0].toUpperCase().trim().replaceAll("\\s+", " ");
                    String lastName = values[1].toUpperCase().trim().replaceAll("\\s+", " ");
                    double weight = 0;
                    double height = 0;

                    if (values.length > 2 && values[2] != null) {
                        // Troca a virgula por ponto e transforma em um número.
                        weight = Double.parseDouble(values[2].replace(",", "."));
                    }

                    if (values.length > 3 && values[3] != null) {
                        // Troca a virgula por ponto e transforma em um número.
                        height = Double.parseDouble(values[3].replace(",", "."));
                    }

                    Pessoa pessoa = new Pessoa(firstName, lastName, weight, height);

                    pessoa.calculaIMC();

                    // Valida se o IMC é um número.
                    if (!Double.isNaN(pessoa.getIMC())) {
                        String result = pessoa.getFirstName() + " " + pessoa.getLastName() + " " + df.format(pessoa.getIMC());
                        System.out.println(result);
                        writer.append(result + "\n");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
