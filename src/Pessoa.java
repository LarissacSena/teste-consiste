class Pessoa {
    private String firstName;
    private String lastName;
    private double weight;
    private double height;
    private double imc;

    // Construtor do objeto pessoa.
    public Pessoa(String firstName, String lastName, double weight, double height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.weight = weight;
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getIMC() {
        return imc;
    }

    public void calculaIMC() {
        this.imc = this.weight / (this.height * this.height);
    }
}